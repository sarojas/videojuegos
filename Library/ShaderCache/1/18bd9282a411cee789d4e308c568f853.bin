�A                         SPOT   SHADOWS_DEPTH      LIGHTMAP_SHADOW_MIXING     SHADOWS_SHADOWMASK  
   _NORMALMAP  �#  ���$      0                       xlatMtlMain #include <metal_stdlib>
#include <metal_texture>
using namespace metal;

#if !(__HAVE_FMA__)
#define fma(a,b,c) ((a) * (b) + (c))
#endif

#ifndef XLT_REMAP_O
	#define XLT_REMAP_O {0, 1, 2, 3, 4, 5, 6, 7}
#endif
constexpr constant uint xlt_remap_o[] = XLT_REMAP_O;
struct FGlobals_Type
{
    float3 _WorldSpaceCameraPos;
    float4 unity_OcclusionMaskSelector;
    float4 hlslcc_mtx4x4unity_WorldToShadow[16];
    float4 _LightShadowData;
    float4 unity_ShadowFadeCenterAndType;
    float4 hlslcc_mtx4x4unity_MatrixV[4];
    float4 _LightColor0;
    float4 _SpecColor;
    float4 _Color;
    float _BumpScale;
    float _Glossiness;
    float4 hlslcc_mtx4x4unity_WorldToLight[4];
};

struct Mtl_FragmentIn
{
    float4 TEXCOORD0 [[ user(TEXCOORD0) ]] ;
    float4 TEXCOORD1 [[ user(TEXCOORD1) ]] ;
    float4 TEXCOORD2 [[ user(TEXCOORD2) ]] ;
    float4 TEXCOORD3 [[ user(TEXCOORD3) ]] ;
    float4 TEXCOORD4 [[ user(TEXCOORD4) ]] ;
    float3 TEXCOORD5 [[ user(TEXCOORD5) ]] ;
    float4 TEXCOORD7 [[ user(TEXCOORD7) ]] ;
};

struct Mtl_FragmentOut
{
    float4 SV_Target0 [[ color(xlt_remap_o[0]) ]];
};

constexpr sampler _mtl_xl_shadow_sampler(address::clamp_to_edge, filter::linear, compare_func::greater_equal);
fragment Mtl_FragmentOut xlatMtlMain(
    constant FGlobals_Type& FGlobals [[ buffer(0) ]],
    sampler samplerunity_ShadowMask [[ sampler (0) ]],
    sampler sampler_ShadowMapTexture [[ sampler (1) ]],
    sampler sampler_MainTex [[ sampler (2) ]],
    sampler sampler_BumpMap [[ sampler (3) ]],
    sampler sampler_LightTexture0 [[ sampler (4) ]],
    sampler sampler_LightTextureB0 [[ sampler (5) ]],
    texture2d<float, access::sample > _MainTex [[ texture(0) ]] ,
    texture2d<float, access::sample > _BumpMap [[ texture(1) ]] ,
    texture2d<float, access::sample > _LightTexture0 [[ texture(2) ]] ,
    texture2d<float, access::sample > _LightTextureB0 [[ texture(3) ]] ,
    texture2d<float, access::sample > unity_ShadowMask [[ texture(4) ]] ,
    depth2d<float, access::sample > _ShadowMapTexture [[ texture(5) ]] ,
    Mtl_FragmentIn input [[ stage_in ]])
{
    Mtl_FragmentOut output;
    float3 u_xlat0;
    float4 u_xlat1;
    float3 u_xlat2;
    bool u_xlatb2;
    float3 u_xlat3;
    float3 u_xlat4;
    float3 u_xlat5;
    float u_xlat6;
    float u_xlat7;
    float u_xlat10;
    bool u_xlatb10;
    float u_xlat11;
    float u_xlat12;
    float u_xlat15;
    float u_xlat16;
    u_xlat0.xyz = input.TEXCOORD5.xyz + (-FGlobals.unity_ShadowFadeCenterAndType.xyz);
    u_xlat0.x = dot(u_xlat0.xyz, u_xlat0.xyz);
    u_xlat0.x = sqrt(u_xlat0.x);
    u_xlat5.xyz = (-input.TEXCOORD5.xyz) + FGlobals._WorldSpaceCameraPos.xyzx.xyz;
    u_xlat1.x = FGlobals.hlslcc_mtx4x4unity_MatrixV[0].z;
    u_xlat1.y = FGlobals.hlslcc_mtx4x4unity_MatrixV[1].z;
    u_xlat1.z = FGlobals.hlslcc_mtx4x4unity_MatrixV[2].z;
    u_xlat5.x = dot(u_xlat5.xyz, u_xlat1.xyz);
    u_xlat0.x = (-u_xlat5.x) + u_xlat0.x;
    u_xlat0.x = fma(FGlobals.unity_ShadowFadeCenterAndType.w, u_xlat0.x, u_xlat5.x);
    u_xlat0.x = fma(u_xlat0.x, FGlobals._LightShadowData.z, FGlobals._LightShadowData.w);
    u_xlat0.x = clamp(u_xlat0.x, 0.0f, 1.0f);
    u_xlat1 = input.TEXCOORD5.yyyy * FGlobals.hlslcc_mtx4x4unity_WorldToShadow[1];
    u_xlat1 = fma(FGlobals.hlslcc_mtx4x4unity_WorldToShadow[0], input.TEXCOORD5.xxxx, u_xlat1);
    u_xlat1 = fma(FGlobals.hlslcc_mtx4x4unity_WorldToShadow[2], input.TEXCOORD5.zzzz, u_xlat1);
    u_xlat1 = u_xlat1 + FGlobals.hlslcc_mtx4x4unity_WorldToShadow[3];
    u_xlat5.xyz = u_xlat1.xyz / u_xlat1.www;
    u_xlat5.x = _ShadowMapTexture.sample_compare(_mtl_xl_shadow_sampler, u_xlat5.xy, saturate(u_xlat5.z), level(0.0));
    u_xlat10 = (-FGlobals._LightShadowData.x) + 1.0;
    u_xlat5.x = fma(u_xlat5.x, u_xlat10, FGlobals._LightShadowData.x);
    u_xlat0.x = u_xlat0.x + u_xlat5.x;
    u_xlat0.x = clamp(u_xlat0.x, 0.0f, 1.0f);
    u_xlat1 = unity_ShadowMask.sample(samplerunity_ShadowMask, input.TEXCOORD7.xy);
    u_xlat5.x = dot(u_xlat1, FGlobals.unity_OcclusionMaskSelector);
    u_xlat5.x = clamp(u_xlat5.x, 0.0f, 1.0f);
    u_xlat0.x = min(u_xlat5.x, u_xlat0.x);
    u_xlat1 = input.TEXCOORD5.yyyy * FGlobals.hlslcc_mtx4x4unity_WorldToLight[1];
    u_xlat1 = fma(FGlobals.hlslcc_mtx4x4unity_WorldToLight[0], input.TEXCOORD5.xxxx, u_xlat1);
    u_xlat1 = fma(FGlobals.hlslcc_mtx4x4unity_WorldToLight[2], input.TEXCOORD5.zzzz, u_xlat1);
    u_xlat1 = u_xlat1 + FGlobals.hlslcc_mtx4x4unity_WorldToLight[3];
    u_xlat5.xy = u_xlat1.xy / u_xlat1.ww;
    u_xlat5.xy = u_xlat5.xy + float2(0.5, 0.5);
    u_xlat5.x = _LightTexture0.sample(sampler_LightTexture0, u_xlat5.xy).w;
    u_xlatb10 = 0.0<u_xlat1.z;
    u_xlat15 = dot(u_xlat1.xyz, u_xlat1.xyz);
    u_xlat15 = _LightTextureB0.sample(sampler_LightTextureB0, float2(u_xlat15)).x;
    u_xlat10 = u_xlatb10 ? 1.0 : float(0.0);
    u_xlat5.x = u_xlat5.x * u_xlat10;
    u_xlat5.x = u_xlat15 * u_xlat5.x;
    u_xlat0.x = u_xlat0.x * u_xlat5.x;
    u_xlat0.xyz = u_xlat0.xxx * FGlobals._LightColor0.xyz;
    u_xlat1.xyz = _BumpMap.sample(sampler_BumpMap, input.TEXCOORD0.xy).xyw;
    u_xlat1.x = u_xlat1.z * u_xlat1.x;
    u_xlat1.xy = fma(u_xlat1.xy, float2(2.0, 2.0), float2(-1.0, -1.0));
    u_xlat1.xy = u_xlat1.xy * float2(FGlobals._BumpScale);
    u_xlat2.xyz = u_xlat1.yyy * input.TEXCOORD3.xyz;
    u_xlat2.xyz = fma(input.TEXCOORD2.xyz, u_xlat1.xxx, u_xlat2.xyz);
    u_xlat15 = dot(u_xlat1.xy, u_xlat1.xy);
    u_xlat15 = min(u_xlat15, 1.0);
    u_xlat15 = (-u_xlat15) + 1.0;
    u_xlat15 = sqrt(u_xlat15);
    u_xlat1.xyz = fma(input.TEXCOORD4.xyz, float3(u_xlat15), u_xlat2.xyz);
    u_xlat15 = dot(u_xlat1.xyz, u_xlat1.xyz);
    u_xlat15 = rsqrt(u_xlat15);
    u_xlat1.xyz = float3(u_xlat15) * u_xlat1.xyz;
    u_xlat15 = dot(input.TEXCOORD1.xyz, input.TEXCOORD1.xyz);
    u_xlat15 = rsqrt(u_xlat15);
    u_xlat2.xyz = float3(u_xlat15) * input.TEXCOORD1.xyz;
    u_xlat3.x = input.TEXCOORD2.w;
    u_xlat3.y = input.TEXCOORD3.w;
    u_xlat3.z = input.TEXCOORD4.w;
    u_xlat15 = dot(u_xlat3.xyz, u_xlat3.xyz);
    u_xlat15 = rsqrt(u_xlat15);
    u_xlat4.xyz = fma(u_xlat3.xyz, float3(u_xlat15), (-u_xlat2.xyz));
    u_xlat16 = dot(u_xlat1.xyz, (-u_xlat2.xyz));
    u_xlat2.xyz = float3(u_xlat15) * u_xlat3.xyz;
    u_xlat15 = dot(u_xlat4.xyz, u_xlat4.xyz);
    u_xlat15 = max(u_xlat15, 0.00100000005);
    u_xlat15 = rsqrt(u_xlat15);
    u_xlat3.xyz = float3(u_xlat15) * u_xlat4.xyz;
    u_xlat15 = dot(u_xlat1.xyz, u_xlat3.xyz);
    u_xlat15 = clamp(u_xlat15, 0.0f, 1.0f);
    u_xlat1.x = dot(u_xlat1.xyz, u_xlat2.xyz);
    u_xlat1.x = clamp(u_xlat1.x, 0.0f, 1.0f);
    u_xlat6 = dot(u_xlat2.xyz, u_xlat3.xyz);
    u_xlat6 = clamp(u_xlat6, 0.0f, 1.0f);
    u_xlat11 = (-FGlobals._Glossiness) + 1.0;
    u_xlat2.x = u_xlat11 * u_xlat11;
    u_xlat2.x = max(u_xlat2.x, 0.00200000009);
    u_xlat7 = u_xlat2.x * u_xlat2.x;
    u_xlat12 = fma(u_xlat15, u_xlat7, (-u_xlat15));
    u_xlat15 = fma(u_xlat12, u_xlat15, 1.0);
    u_xlat15 = fma(u_xlat15, u_xlat15, 1.00000001e-07);
    u_xlat7 = u_xlat7 * 0.318309873;
    u_xlat15 = u_xlat7 / u_xlat15;
    u_xlat7 = (-u_xlat2.x) + 1.0;
    u_xlat12 = fma(abs(u_xlat16), u_xlat7, u_xlat2.x);
    u_xlat2.x = fma(u_xlat1.x, u_xlat7, u_xlat2.x);
    u_xlat2.x = abs(u_xlat16) * u_xlat2.x;
    u_xlat16 = -abs(u_xlat16) + 1.0;
    u_xlat2.x = fma(u_xlat1.x, u_xlat12, u_xlat2.x);
    u_xlat2.x = u_xlat2.x + 9.99999975e-06;
    u_xlat2.x = 0.5 / u_xlat2.x;
    u_xlat15 = u_xlat15 * u_xlat2.x;
    u_xlat15 = u_xlat1.x * u_xlat15;
    u_xlat15 = u_xlat15 * 3.14159274;
    u_xlat15 = max(u_xlat15, 0.0);
    u_xlat2.x = dot(FGlobals._SpecColor.xyz, FGlobals._SpecColor.xyz);
    u_xlatb2 = u_xlat2.x!=0.0;
    u_xlat2.x = u_xlatb2 ? 1.0 : float(0.0);
    u_xlat15 = u_xlat15 * u_xlat2.x;
    u_xlat2.xyz = u_xlat0.xyz * float3(u_xlat15);
    u_xlat15 = (-u_xlat6) + 1.0;
    u_xlat6 = u_xlat6 * u_xlat6;
    u_xlat6 = dot(float2(u_xlat6), float2(u_xlat11));
    u_xlat6 = u_xlat6 + -0.5;
    u_xlat11 = u_xlat15 * u_xlat15;
    u_xlat11 = u_xlat11 * u_xlat11;
    u_xlat15 = u_xlat15 * u_xlat11;
    u_xlat3.xyz = (-FGlobals._SpecColor.xyz) + float3(1.0, 1.0, 1.0);
    u_xlat3.xyz = fma(u_xlat3.xyz, float3(u_xlat15), FGlobals._SpecColor.xyz);
    u_xlat2.xyz = u_xlat2.xyz * u_xlat3.xyz;
    u_xlat15 = u_xlat16 * u_xlat16;
    u_xlat15 = u_xlat15 * u_xlat15;
    u_xlat15 = u_xlat16 * u_xlat15;
    u_xlat15 = fma(u_xlat6, u_xlat15, 1.0);
    u_xlat11 = (-u_xlat1.x) + 1.0;
    u_xlat16 = u_xlat11 * u_xlat11;
    u_xlat16 = u_xlat16 * u_xlat16;
    u_xlat11 = u_xlat11 * u_xlat16;
    u_xlat6 = fma(u_xlat6, u_xlat11, 1.0);
    u_xlat15 = u_xlat15 * u_xlat6;
    u_xlat15 = u_xlat1.x * u_xlat15;
    u_xlat0.xyz = float3(u_xlat15) * u_xlat0.xyz;
    u_xlat15 = max(FGlobals._SpecColor.y, FGlobals._SpecColor.x);
    u_xlat15 = max(u_xlat15, FGlobals._SpecColor.z);
    u_xlat15 = (-u_xlat15) + 1.0;
    u_xlat1.xyz = _MainTex.sample(sampler_MainTex, input.TEXCOORD0.xy).xyz;
    u_xlat1.xyz = u_xlat1.xyz * FGlobals._Color.xyz;
    u_xlat1.xyz = float3(u_xlat15) * u_xlat1.xyz;
    output.SV_Target0.xyz = fma(u_xlat1.xyz, u_xlat0.xyz, u_xlat2.xyz);
    output.SV_Target0.w = 1.0;
    return output;
}
                                 FGlobals         _WorldSpaceCameraPos                         unity_OcclusionMaskSelector                         _LightShadowData                        unity_ShadowFadeCenterAndType                     0     _LightColor0                  �  
   _SpecColor                    �     _Color                    �  
   _BumpScale                    �     _Glossiness                   �     unity_WorldToShadow                        unity_MatrixV                    @     unity_WorldToLight                   �            _MainTex                 _BumpMap                _LightTexture0                  _LightTextureB0                 unity_ShadowMask                 _ShadowMapTexture                   FGlobals           