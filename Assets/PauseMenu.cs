﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PauseMenu : MonoBehaviour {

    public static bool GameIsPaused = false;
    public GameObject pauseMenuUI;
	// Update is called once per frame
	void Update () 
    {

        if ( Input.GetKeyDown(KeyCode.P))
        {
            if (GameIsPaused)
            {
                Resume();
                Debug.Log("resume");

            }else
            {
                Pause();
                Debug.Log("Pause");
            }
        }
	}

    public void Resume()
    {
        pauseMenuUI.SetActive(false);
        Time.timeScale = 1f;
        GameIsPaused = false;
    }

    void Pause()
    {
        pauseMenuUI.SetActive(true);
        Time.timeScale = 0f;
        GameIsPaused = true;
    }

    public void LoadMenu(int load)
    {
        Debug.Log("LOAD SCENE");
        SceneManager.LoadScene(1);

    }

    public void QuitGame (int exit)
    {
        Debug.Log("EXIT");
        Application.Quit();
    }
}
