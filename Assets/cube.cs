﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class cube : MonoBehaviour 
{
    
    public Text scoreText;
    private int puntaje;
    public Text HighScore;
    private int puntaje_max;


    void Start () 
    {
        HighScore.text = "High Score " + PlayerPrefs.GetInt("HighScore", 0);
        puntaje_max = PlayerPrefs.GetInt("HighScore", 0);

        //Debug.Log(puntaje_max);
	}

    void Update()
    {
        if ((int)(transform.position.y) > puntaje)
        {
            puntaje = (int)(transform.position.y);
            scoreText.text = "Score " + puntaje;
            //PlayerPrefs.SetInt("HighScore", puntaje);
            if (puntaje > puntaje_max)
            {
                PlayerPrefs.SetInt("HighScore", puntaje);
                HighScore.text = "High Score " + puntaje;
            }
        }

    }

       
}


