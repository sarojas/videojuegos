﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.Characters.FirstPerson;

public class PowerUp_1: MonoBehaviour {

    public GameObject pickupEffect;
    public float duration = 5f;
    private GameObject clip1;
    private GameObject clip2;
    private GameObject velocidad;

	void OnTriggerEnter (Collider other)
	{
        if( other.CompareTag("Player") )
        {
            StartCoroutine( Pickup(other) ) ;    
        }
	}

    IEnumerator Pickup(Collider Player){

        clip1 = GameObject.FindGameObjectWithTag("clip_1");
        clip2 = GameObject.FindGameObjectWithTag("clip_2");
        velocidad = GameObject.FindGameObjectWithTag("Player");


        Instantiate(pickupEffect, transform.position  , transform.rotation);
        
        GetComponent<MeshRenderer>().enabled = false;
        GetComponent<Collider>().enabled = false;


        //crear accion
        
        //GameObject.Find("Player").transform.localScale = new Vector3(2f, 2f, 2f);
        velocidad.GetComponent<FirstPersonController>().m_WalkSpeed = 6;
        velocidad.GetComponent<FirstPersonController>().m_RunSpeed = 10;
        clip1.GetComponent<AudioSource>().Play();
        
        yield return new WaitForSeconds (duration);

        //revertir accion
        //GameObject.Find("Player").transform.localScale = new Vector3(1f, 1f, 1f);
        velocidad.GetComponent<FirstPersonController>().m_WalkSpeed = 3;
        velocidad.GetComponent<FirstPersonController>().m_RunSpeed = 7;
        clip2.GetComponent<AudioSource>().Play();
            


        //Debug.Log("Player pickup");
        Destroy(gameObject);
    }



}
