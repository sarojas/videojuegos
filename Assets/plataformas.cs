﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;

public class plataformas : MonoBehaviour
{
    public Transform plataforma1;
    public Transform plataforma2;
    public Transform plataforma3;
    public Transform PowerUp1;
    public Transform PowerUp2;
    public Transform Piso2;
    public Transform Piso3;
    private bool ifPiso2;
    private bool ifPiso3;
    private bool ifPowerUp1;
    private int rango; // plataformas a crear por tramo
    public float posCube;
    private int tramo;
    public float distancia; //distancia entre plataformas
    private int probPowerUp;
    public int rangoPowerups;
    private float xAux;
    private float zAux;
    private float xTemp;
    private float zTemp;
    private float xTemp2;
    private float zTemp2;
    private bool avanzar;

    void Start()
    {
        rango = 10;
        xAux = -7;  //localisacion primera plataforma
        zAux = -20;
        ifPiso2 = false;
        ifPowerUp1 = false;
        tramo = 1; // contador de een que tramod e plataaformas va
        for (int i = 4  ; i < rango; i++) //crea las plataformas dentro del rango
        {
            Instantiate(plataforma1, new Vector3(xAux, i, zAux), Quaternion.identity); // instancia el prefab
            if(i != 4)
            {
                xTemp2 = xTemp; // guarda posicion de la antepenultima
                zTemp2 = zTemp;
            }            
            xTemp = xAux; //guarda la posicion de la ultima plataforma
            zTemp = zAux;
            xAux += UnityEngine.Random.Range(-distancia, distancia);   //Da un valor aleatorio a la posicion de la plataforma 
            zAux += UnityEngine.Random.Range(-distancia, distancia);

            while (Math.Abs(xAux) < 50 && Math.Abs(Math.Abs(xAux) - Math.Abs(xTemp)) < 2 && Math.Abs(Math.Abs(xAux) - Math.Abs(xTemp2)) < 2)  // verifica que no este encima de  la plaataforma pasada
            {
                xAux = xTemp + UnityEngine.Random.Range(-distancia, distancia);
            }
            while (Math.Abs(zAux) < 50 && Math.Abs(Math.Abs(zAux) - Math.Abs(zTemp)) < 2 && Math.Abs(Math.Abs(zAux) - Math.Abs(zTemp2)) > 2)
            {
                zAux = zTemp + UnityEngine.Random.Range(-distancia, distancia);
            }
        }
    }
    void Update()
    {
        posCube = GameObject.Find("Player").transform.position.y;

        if (posCube >= rango * tramo - rango && ifPiso3 == true)  //PISO 3 Crea plataformas cada vez quee stemos en rango
        {
            for (float i = rango * tramo; i < rango * tramo + rango; i++) //genera las plataformas en el tramo actual
            {
                Instantiate(plataforma3, new Vector3(xAux, i , zAux), Quaternion.Euler(90, 0, 0)); // instancia el prefa
                probPowerUp = UnityEngine.Random.Range(-rangoPowerups, rangoPowerups);
                if (probPowerUp == 5)
                {
                    Instantiate(PowerUp1, new Vector3(xAux, i + 0.5f, zAux), Quaternion.identity); // instancia el prefa
                }
                else if (probPowerUp == -5)
                {
                    Instantiate(PowerUp2, new Vector3(xAux, i + 0.5f, zAux), Quaternion.identity); // instancia el prefa
                }
                xTemp = xAux; //guarda la posicion de la ultima plataforma
                zTemp = zAux;
                avanzar = false;
                while (avanzar == false)  // verifica que no este encima de  la plaataforma pasada
                {
                    xAux = xTemp + UnityEngine.Random.Range(-distancia - 0.9f, distancia + 0.9f);
                    avanzar = Math.Abs(xAux) < 48 && Math.Abs(Math.Abs(xAux) - Math.Abs(xTemp)) > 2 /*&& Math.Abs(Math.Abs(xAux) - Math.Abs(xTemp2)) > 2*/;

                }
                avanzar = false;
                while (avanzar == false)
                {
                    zAux = zTemp + UnityEngine.Random.Range(-distancia - 0.9f, distancia + 0.9f);
                    avanzar = Math.Abs(zAux) < 48 && Math.Abs(Math.Abs(zAux) - Math.Abs(zTemp)) > 2 /*&& Math.Abs(Math.Abs(zAux) - Math.Abs(zTemp2)) > 2*/;

                }
                avanzar = false;
            }
            tramo += 1; // avanza al siguiente tramo
        }
        else if (posCube >= rango * tramo - rango && ifPiso2 == true)  //PISO 2 | Crea plataformas cada vez quee stemos en rango
        {
            for (float i = rango * tramo; i < rango * tramo + rango; i++) //genera las plataformas en el tramo actual
            {
                if (posCube > 130)
                {
                    Instantiate(plataforma3, new Vector3(xAux, i , zAux), Quaternion.Euler(90, 0, 0)); // instancia el prefa
                    probPowerUp = UnityEngine.Random.Range(-rangoPowerups, rangoPowerups);
                    if (probPowerUp == 5)
                    {
                        Instantiate(PowerUp1, new Vector3(xAux, i + 0.5f, zAux), Quaternion.identity); // instancia el prefa
                    }
                    else if (probPowerUp == -5)
                    {
                        Instantiate(PowerUp2, new Vector3(xAux, i + 0.5f, zAux), Quaternion.identity); // instancia el prefa
                    }

                }
                else
                {
                    Instantiate(plataforma2, new Vector3(xAux, i , zAux), Quaternion.identity); // instancia el prefa
                    probPowerUp = UnityEngine.Random.Range(-rangoPowerups, rangoPowerups);
                    if (probPowerUp == 5)
                    {
                        Instantiate(PowerUp1, new Vector3(xAux, i + 0.5f, zAux), Quaternion.identity); // instancia el prefa
                    }
                    else if (probPowerUp == -5)
                    {
                        Instantiate(PowerUp2, new Vector3(xAux, i + 0.5f, zAux), Quaternion.identity); // instancia el prefa
                    }
                }
                if (posCube > 135 && ifPiso3 == false)
                {
                    Instantiate(Piso3, new Vector3(0, 140, 0), Quaternion.identity); // instancia el prefa
                    ifPiso3 = true;
                    plataforma2 = plataforma3;
                }
                xTemp = xAux; //guarda la posicion de la ultima plataforma
                zTemp = zAux;
                avanzar = false;
                while (avanzar == false)  // verifica que no este encima de  la plaataforma pasada
                {
                    xAux = xTemp + UnityEngine.Random.Range(-distancia - 0.5f, distancia + 0.5f);
                    avanzar = Math.Abs(xAux) < 48 && Math.Abs(Math.Abs(xAux) - Math.Abs(xTemp)) > 2 /*&& Math.Abs(Math.Abs(xAux) - Math.Abs(xTemp2)) > 2*/;
                }
                avanzar = false;
                while (avanzar == false)
                {
                    zAux = zTemp + UnityEngine.Random.Range(-distancia - 0.5f, distancia + 0.5f);
                    avanzar = Math.Abs(zAux) < 48 && Math.Abs(Math.Abs(zAux) - Math.Abs(zTemp)) > 2 /*&& Math.Abs(Math.Abs(zAux) - Math.Abs(zTemp2)) > 2*/;
                }
                avanzar = false;
            }
            tramo += 1; // avanza al siguiente tramo
        }
        else if (posCube >= rango*tramo - rango)  //PISO 1 | Crea plataformas cada vez quee stemos en rango
        {
            for (float i = rango*tramo; i < rango*tramo + rango; i++) //genera las plataformas en el tramo actual
            {
                if(posCube > 60)
                {
                    Instantiate(plataforma2, new Vector3(xAux, i , zAux), Quaternion.identity); // instancia el prefa
                    probPowerUp = UnityEngine.Random.Range(-rangoPowerups, rangoPowerups);
                    if (probPowerUp == 5)
                    {
                        Instantiate(PowerUp1, new Vector3(xAux, i + 0.5f, zAux), Quaternion.identity); // instancia el prefa
                    }
                    else if (probPowerUp == -5)
                    {
                        Instantiate(PowerUp2, new Vector3(xAux, i + 0.5f, zAux), Quaternion.identity); // instancia el prefa
                    }
                }
                else { 
                    Instantiate(plataforma1, new Vector3(xAux, i , zAux), Quaternion.identity); // instancia el prefa
                    probPowerUp = UnityEngine.Random.Range(-rangoPowerups, rangoPowerups);
                    if (probPowerUp == 5)
                    {
                        Instantiate(PowerUp1, new Vector3(xAux, i +0.5f, zAux), Quaternion.identity); // instancia el prefa
                    }
                    else if (probPowerUp == -5) {
                        Instantiate(PowerUp2, new Vector3(xAux, i + 0.5f, zAux), Quaternion.identity); // instancia el prefa
                    }
                }              
                if(posCube > 65 && ifPiso2 == false){
                    Instantiate(Piso2, new Vector3(0,70,0), Quaternion.identity); // instancia el prefa
                    ifPiso2 = true;
                    
                }
                xTemp = xAux; //guarda la posicion de la ultima plataforma
                zTemp = zAux;
                avanzar = false;
                while (avanzar == false)  // verifica que no este encima de  la plaataforma pasada
                {
                    xAux = xTemp + UnityEngine.Random.Range(-distancia, distancia);
                    avanzar = Math.Abs(xAux) < 48 && Math.Abs(Math.Abs(xAux) - Math.Abs(xTemp)) > 2 /*&& Math.Abs(Math.Abs(xAux) - Math.Abs(xTemp2)) > 2*/;
                    
                }
                avanzar = false;
                while (avanzar == false)
                {
                    zAux = zTemp + UnityEngine.Random.Range(-distancia, distancia);
                    avanzar = Math.Abs(zAux) < 48 && Math.Abs(Math.Abs(zAux) - Math.Abs(zTemp)) > 2 /*&& Math.Abs(Math.Abs(zAux) - Math.Abs(zTemp2)) > 2*/;
                   
                }
                avanzar = false;
            }
            tramo += 1; // avanza al siguiente tramo
        }      
    }
}